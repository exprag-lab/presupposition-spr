function createViews(trials) {
    const intro = babeViews.intro({
        name: 'intro',
        trials: 1,
        title: 'Herzlich Willkommen zum XPrag Experiment!',
        text: 'Vielen Dank, dass Sie an unserem Experiment teilnehmen. Im Folgenden werden wir Ihnen kurz erklären, wie das Experiment abläuft. Bitte klicken Sie dazu auf den Start Button.'
    });

    const instructions = babeViews.instructions({
        name: 'instructions',
        trials: 1,
        title: 'Anleitung',
        text: 'Im nachfolgenden Experiment wird Ihnen immer ein Kontextsatz gezeigt, den Sie bitte in Ihrem eigenen Lesetempo lesen. Anschließend drücken Sie bitte die Leertaste, um zum Testsatz zu gelangen. Dieser wird Ihnen wortweise präsentiert. Zu Beginn sehen Sie nur einzelne Striche. Sobald Sie die Leertaste drücken erscheint das erste Wort. Drücken Sie die Leertaste erneut, so verschwindet das erste Wort und das zweite Wort erscheint und so weiter bis zum Ende des Satzes. Die Anzahl der Striche entspricht der Anzahl der Worte des Satzes. Bitte legen Sie während des Lesens einen Finger auf die Leertaste und lesen Sie den Satz in Ihrem natürlichen Lesetempo, so dass Sie den Inhalt gut verstehen können. Nachdem Sie den Satz vollständig gelesen haben erscheint eine Skala auf der Sie bitte die Natürlichkeit des Satzes im jeweiligen Kontext bewerten sollen (ganz links sehr unnatürlich, ganz rechts sehr natürlich). Klicken Sie dazu mit der Maus auf den entsprechenden Kreis. Um zum nächsten Satz zu gelangen klicken Sie bitte auf weiter. Es folgen einige Übungsdurchgänge.',
        buttonText: 'Weiter zu den Übungsdurchgängen'
    });

    const begin = babeViews.begin({
        name: 'begin',
        trials: 1,
        title: '',
        text: 'Das waren die Übungsdurchgänge. Nun beginnen wir mit dem Experiment. Bitte lesen sie die Sätze aufmerksam und in Ihrem natürlichen Lesetempo. Viel Spaß bei dem Experiment. Um zu Beginnen klicken Sie bitte auf den Button „Experiment starten“',
        buttonText: 'Experiment starten'
    });

    const practice = babeViews.selfPacedReading_ratingScale({
        name: 'main',
        title: 'Übungsdurchgänge',
        trials: 2,
        trial_type: 'spr_practice',
        data: trials.practice,
        pause: 500,
        fix_duration: 500,
        // declared in events.js
        custom_events: {
            after_pause: evts.addContext,
            after_stim_hidden: evts.hideQUD
        }
    });

    const main = babeViews.selfPacedReading_ratingScale({
        name: 'main',
        trials: 30,
        trial_type: 'spr_main',
        data: trials.main,
        pause: 500,
        fix_duration: 500,
        // declared in events.js
        custom_events: {
            after_pause: evts.addContext,
            after_stim_hidden: evts.hideQUD
        }
    });

    const postTest = babeViews.postTest({
        name: 'postTest',
        trials: 1,
        title: 'Weitere Angaben',
        text: 'Die Beantwortung der folgenden Fragen ist optional, aber es kann bei der Auswertung hilfreich sein, damit wir Ihre Antowrten besser verstehen.',
        buttonText: 'Weiter',
        age_question: 'Alter',
        gender_question: 'Geschlecht',
        gender_male: 'männlich',
        gender_female: 'weiblich',
        gender_other: 'sonstig',
        edu_question: 'Höchster Bildungsabschluss',
        edu_graduated_high_school: 'Abitur',
        edu_graduated_college: 'Hochschulabschluss',
        edu_higher_degree: 'Universitärer Abschluss',
        languages_question: 'Muttersprache',
        languages_more: '(in der Regel die Sprache, die Sie als Kind zu Hause gesprochen haben)',
        comments_question: 'Weitere Kommentare'
    });

    const thanks = babeViews.thanks({
        name: 'thanks',
        trials: 1,
        title: 'Vielen Dank für Ihre Teilnahme!'
    });

    return [
        intro,
        instructions,
        practice,
        begin,
        main,
        postTest,
        thanks
    ];
};

