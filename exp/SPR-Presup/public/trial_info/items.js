// prepares the trial object in the format needed for the view
const prepareTrials = function() {
    const trials = {};
    let loadedTrials = loadTrials();
    let preparedItems = loadedTrials.reduce((final, item) => {
        let v = {
            QUD: 'Drücken Sie die Leertaste, wenn Sie den Satz gelesen haben',
            type: 'verifying',
            help_text: 'Drücken Sie die Leertaste, damit die Worte erscheinen',
            context: item['verifying'],
            sentence: item['test-v'].split(' ').join(' | '),
            trigger: item['trigger'],
            number: item['number'],
            triggerPosition: item['triggerPosition'],
            evaluationPosition: item['evaluationPosition'],
            optionLeft: 'Sehr unnatürlich',
            optionRight: 'Sehr natürlich'
        };
        let f = {
            QUD: 'Drücken Sie die Leertaste, wenn Sie den Satz gelesen haben',
            type: 'falsifying',
            help_text: 'Drücken Sie die Leertaste, damit die Worte erscheinen',
            context: item['falsifying'],
            sentence: item['test-f'].split(' ').join(' | '),
            trigger: item['trigger'],
            number: item['number'],
            triggerPosition: item['triggerPosition'],
            evaluationPosition: item['evaluationPosition'],
            optionLeft: 'Sehr unnatürlich',
            optionRight: 'Sehr natürlich'
        };
        let n = {
            QUD: 'Drücken Sie die Leertaste, wenn Sie den Satz gelesen haben',
            type: 'neutral',
            help_text: 'Drücken Sie die Leertaste, damit die Worte erscheinen',
            context: item['neutral'],
            sentence: item['test-n'].split(' ').join(' | '),
            trigger: item['trigger'],
            number: item['number'],
            triggerPosition: item['triggerPosition'],
            evaluationPosition: item['evaluationPosition'],
            optionLeft: 'Sehr unnatürlich',
            optionRight: 'Sehr natürlich'
        };
        final.push(v, f, n);
        return final;
    }, []);

    trials.practice = [
        {
            QUD: 'Drücken Sie die Leertaste, wenn Sie den Satz gelesen haben',
            help_text: 'Drücken Sie die Leertaste, damit die Worte erscheinen',
            context: "Rudolf hat letzte Woche Pinguine gefüttert.",
            sentence: "Heute hat Rudolf wieder Pinguine gefüttert und freut sich.".split(' ').join(' | '),
            trigger: "wieder",
            type: "verifying",
            optionLeft: 'Sehr unnatürlich',
            optionRight: 'Sehr natürlich'
        },
        {
            QUD: 'Drücken Sie die Leertaste, wenn Sie den Satz gelesen haben',
            help_text: 'Drücken Sie die Leertaste, damit die Worte erscheinen',
            context: "Holger besitzt kein Restaurant.",
            sentence: "Susanne geht in sein Restaurant und trifft Bekannte.".split(' ').join(' | '),
            trigger: "sein",
            type: "falsifying",
            optionLeft: 'Sehr unnatürlich',
            optionRight: 'Sehr natürlich'
        }
    ];

    while (true) {
        try {
            trials.main = shuffleItems(preparedItems);
            break;
        } catch (e) {
            console.debug(e);
        }
    }

    return trials;
};

// picks names
const prepareNames = function() {
    let trials_meta = {'wieder': 12, 'wissen': 12, 'aufhören': 12, 'sein': 12, 'auch': 12};
    let names = {};

    // Type_1: (wissen, wieder, aufhören) Ver_male_var_j, Fals_male_var_k, Test_male_var_j/k/n (neutral male name kept constant)
    // Type_2: (sein) Ver_male_var_j, Fals_male_var_k, Neutr_male_var_n (no name variable in test-sentence)
    // Type_3: (auch) Ver_female_var_j, Fals_female_var_k, Neutr_female_var_n, Test_female_var_j/k/n
    let pickNames = function() {
        var pickedNames = {};

        for (trigger in trials_meta) {
            // Type 1 :: male names
            if (trials_meta.hasOwnProperty(trigger) && (trigger === 'wieder') || (trigger === 'wissen') || (trigger === 'aufhören')) {
                for (var i = 0; i < trials_meta[trigger]; i++) {
                    var names = {};
                    var gmn = ["Peter", "Anton", "Stefan", "Patrick", "Nico", "David", "Uwe", "Martin", "Simon", "Holger", "Jakob", "Luca", "Marcel", "Norbert", "Olaf", "Pascal", "Robin", "Werner", "Otto", "Rudolf", "Thorsten"];
                    names["v_name"] = gmn[Math.floor(Math.random()*gmn.length)];
                    gmn.splice(gmn.indexOf(names[0]), 1);
                    names["f_name"] = gmn[Math.floor(Math.random()*gmn.length)];
                    gmn.splice(gmn.indexOf(names[1]), 1);
                    names["n_name"] = gmn[Math.floor(Math.random()*gmn.length)];

                    if (pickedNames[trigger] === undefined) {
                        pickedNames[trigger] = [];
                        pickedNames[trigger].push(names);
                    } else {
                        pickedNames[trigger].push(names);
                    }
                }
            }
            // Type 2 :: male names
            else if (trials_meta.hasOwnProperty(trigger) && trigger === 'sein') {
                for (var i = 0; i < trials_meta[trigger]; i++) {
                    var names = {};
                    var gmn = ["Peter", "Anton", "Stefan", "Patrick", "Nico", "David", "Uwe", "Martin", "Simon", "Holger", "Jakob", "Luca", "Marcel", "Norbert", "Olaf", "Pascal", "Robin", "Werner", "Otto", "Rudolf", "Thorsten"];
                    names["v_name"] = gmn[Math.floor(Math.random()*gmn.length)];
                    gmn.splice(gmn.indexOf(names["v_name"]), 1);
                    names["f_name"] = gmn[Math.floor(Math.random()*gmn.length)];
                    gmn.splice(gmn.indexOf(names["f_name"]), 1);
                    names["n_name"] = gmn[Math.floor(Math.random()*gmn.length)];

                    if (pickedNames[trigger] === undefined) {
                        pickedNames[trigger] = [];
                        pickedNames[trigger].push(names);
                    } else {
                        pickedNames[trigger].push(names);
                    }
                }
            }
            // Type 3 :: female names
            else if (trials_meta.hasOwnProperty(trigger) && trigger === 'auch') {
                for (var i = 0; i < trials_meta[trigger]; i++) {
                    var names = {};
                    var gfn = ["Anna", "Anke", "Lina", "Inge", "Tina", "Vera", "Saskia", "Sigrid", "Edith", "Esther", "Gertrud", "Jana", "Judith", "Karen", "Linda", "Lydia", "Mona", "Olga", "Petra", "Britta", "Laura", "Astrid", "Berta", "Carla", "Dana", "Hannah", "Eva", "Ingrid"];
                    names["v_name"] = gfn[Math.floor(Math.random()*gfn.length)];
                    gfn.splice(gfn.indexOf(names["v_name"]), 1);
                    names["f_name"] = gfn[Math.floor(Math.random()*gfn.length)];
                    gfn.splice(gfn.indexOf(names["f_name"]), 1);
                    names["n_name"] = gfn[Math.floor(Math.random()*gfn.length)];

                    if (pickedNames[trigger] === undefined) {
                        pickedNames[trigger] = [];
                        pickedNames[trigger].push(names);
                    } else {
                        pickedNames[trigger].push(names);
                    }
                }
            } else {
                console.log('no such trigger');
            }
        }

        return pickedNames;
    };

    names = pickNames();

    return names;
};

// shuffles the items and verifies two items of the same trigger don't end up
// next to each other
const shuffleItems = function(items) {
    let trials = [];
    var okay, index, i;

    while (items.length > 0) {
        okay = false;
        for (i = 0; i < 4; i++) {
            index = Math.floor(Math.random() * items.length);

            if ((trials.length === 0) || (trials[trials.length - 1]['trigger'] !== items[index]['trigger'])) {
                trials.push(items[index]);
                items.splice(index, 1);
                okay = true;
                break;
            }
        }

        if (!okay) {
            for (var i = 0; i < trials.length; i++) {
                items.push(trials[i]);
            }

            throw "possible infinite loop";
        }
    }

    return trials;
};

// loads the trials with the added names that are randomly picked
const loadTrials = function() {
    let names = prepareNames();
    let groups = [[], [], [], [], []];
    let lists = [[], [], [], [], [], []];

    let items = [
        {
            "number": 1,
            "triggerPosition" : 4,
            "evaluationPosition" : 6,
            "trigger": "wieder",
            "verifying": names['wieder'][0]['v_name'] + " hat letzte Woche Pinguine gefüttert.",
            "falsifying": names['wieder'][0]['f_name'] + " hat noch nie Pinguine gefüttert.",
            "neutral": "Fritz hat noch nie Pinguine gefüttert.",
            "test-v": "Heute hat " + names['wieder'][0]['v_name'] + " wieder Pinguine gefüttert und freut sich.",
            "test-f": "Heute hat " + names['wieder'][0]['f_name'] + " wieder Pinguine gefüttert und freut sich.",
            "test-n": "Heute hat " + names['wieder'][0]['n_name'] + " wieder Pinguine gefüttert und freut sich."
        },
        {
            "number": 2,
            "triggerPosition" : 4,
            "evaluationPosition" : 7,
            "trigger": "wieder",
            "verifying": names['wieder'][1]['v_name'] + " hat bereits rote Handschuhe gekauft.",
            "falsifying": names['wieder'][1]['f_name'] + " hat bisher nie rote Handschuhe gekauft.",
            "neutral": "Jonas hat bisher nie rote Handschuhe gekauft.",
            "test-v": "Heute hat " + names['wieder'][1]['v_name'] + " wieder rote Handschuhe gekauft und sie gleich angezogen.",
            "test-f": "Heute hat " + names['wieder'][1]['f_name'] + " wieder rote Handschuhe gekauft und sie gleich angezogen.",
            "test-n": "Heute hat " + names['wieder'][1]['n_name'] + " wieder rote Handschuhe gekauft und sie gleich angezogen."
        },
        {
            "number": 3,
            "triggerPosition" : 5,
            "evaluationPosition" : 9,
            "trigger": "wieder",
            "verifying": names['wieder'][2]['v_name'] + " hat letztes Jahr an einem Marathon teilgenommen.",
            "falsifying": names['wieder'][2]['f_name'] + " hat niemals an einem Marathon teilgenommen.",
            "neutral": "Karl hat niemals an einem Marathon teilgenommen.",
            "test-v": "Dieses Jahr hat " + names['wieder'][2]['v_name'] + " wieder an einem Marathon teilgenommen und ist stolz.",
            "test-f": "Dieses Jahr hat " + names['wieder'][2]['f_name'] + "  wieder an einem Marathon teilgenommen und ist stolz.",
            "test-n": "Dieses Jahr hat " + names['wieder'][2]['n_name'] + " wieder an einem Marathon teilgenommen und ist stolz."
        },
        {
            "number": 4,
            "triggerPosition" : 5,
            "evaluationPosition" : 10,
            "trigger": "wieder",         
            "verifying": names['wieder'][3]['v_name'] + " ist oft eine Auszeichnung als bester Mitarbeiter verliehen worden.",
            "falsifying": names['wieder'][3]['f_name'] + " ist bis dato eine Auszeichnung als bester Mitarbeiter immer entgangen.",
            "neutral": "Jan ist oft eine Auszeichnung als bester Mitarbeiter verliehen worden.",
            "test-v": "Letzten Montag ist " + names['wieder'][3]['v_name'] + " wieder als bester Mitarbeiter ausgezeichnet worden und das freut ihn.",
            "test-f": "Letzten Montag ist " + names['wieder'][3]['v_name'] + " wieder als bester Mitarbeiter ausgezeichnet worden und das freut ihn.",
            "test-n": "Letzten Montag ist " + names['wieder'][3]['n_name'] + " wieder als bester Mitarbeiter ausgezeichnet worden und das freut ihn."
        },
        {
            "number": 5,
            "triggerPosition" : 4,
            "evaluationPosition" : 10,
            "trigger": "wieder",
            "verifying": names['wieder'][4]['v_name'] + " hat jedes Angebot für eine Lebensversicherung abgelehnt.",
            "falsifying": names['wieder'][4]['f_name'] + " hat vor zehn Jahren ein Angebot für eine Lebensversicherung angenommen.",
            "neutral": "Fritz hat jedes Angebot für eine Lebensversicherung abgelehnt.",
            "test-v": "Gestern hat " + names['wieder'][4]['v_name'] + " wieder ein Angebot für eine Lebensversicherung abgelehnt, das ihm unterbreitet worden ist.",
            "test-f": "Gestern hat " + names['wieder'][4]['f_name'] + " wieder ein Angebot für eine Lebensversicherung abgelehnt, das ihm unterbreitet worden ist.",
            "test-n": "Gestern hat " + names['wieder'][4]['n_name'] + " wieder ein Angebot für eine Lebensversicherung abgelehnt, das ihm unterbreitet worden ist."
        },
        {
            "number": 6,
            "triggerPosition" : 5,
            "evaluationPosition" : 8,
            "trigger": "wieder",
            "verifying": names['wieder'][5]['v_name'] + " hat letztes Jahr Tinas Charme nachgegeben.",
            "falsifying": names['wieder'][5]['f_name'] + " hat Tinas Charme immer widerstanden.",
            "neutral": "Karl hat Inges Charme immer widerstanden.",
            "test-v": "Letzte Woche hat " + names['wieder'][5]['v_name'] + " wieder Tinas Charme nachgegeben und er erzählt es Paul.",
            "test-f": "Letzte Woche hat " + names['wieder'][5]['f_name'] + " wieder Tinas Charme nachgegeben und er erzählt es Paul.",
            "test-n": "Letzte Woche hat " + names['wieder'][5]['n_name'] + " wieder Tinas Charme nachgegeben und er erzählt es Paul."
        },
        {
            "number": 7,
            "triggerPosition" : 5,
            "evaluationPosition" : 8,
            "trigger": "wieder",
            "verifying": names['wieder'][6]['v_name'] + " hat vor zwei Jahren Susannes Geburtstag vergessen.",
            "falsifying": names['wieder'][6]['f_name'] + " hat stets an Susannes Geburtstag gedacht.",
            "neutral": "Karl hat vor zwei Jahren Susannes Geburtstag vergessen.",
            "test-v": "Dieses Jahr hat " + names['wieder'][6]['v_name'] + " wieder Susannes Geburtstag vergessen und er bittet um Entschuldigung.",
            "test-f": "Dieses Jahr hat " + names['wieder'][6]['f_name'] + " wieder Susannes Geburtstag vergessen und er bittet um Entschuldigung.",
            "test-n": "Dieses Jahr hat " + names['wieder'][6]['n_name'] + " wieder Susannes Geburtstag vergessen und er bittet um Entschuldigung."
        },
        {
            "number": 8,
            "triggerPosition" : 5,
            "evaluationPosition" : 8,
            "trigger": "wieder",
            "verifying": names['wieder'][7]['v_name'] + " hat sich letztes Jahr einen Hund gekauft.",
            "falsifying": names['wieder'][7]['f_name'] + " hat sich in seinem ganzen Leben noch keinen Hund gekauft.",
            "neutral": "Jonas hat sich in seinem ganzen Leben noch keinen Hund gekauft.",
            "test-v": "Gestern hat " + names['wieder'][7]['v_name'] + " sich wieder einen Hund gekauft und er nennt ihn Rex.",
            "test-f": "Gestern hat " + names['wieder'][7]['f_name'] + " sich wieder einen Hund gekauft und er nennt ihn Rex.",
            "test-n": "Gestern hat " + names['wieder'][7]['n_name'] + " sich wieder einen Hund gekauft und er nennt ihn Rex."
        },
        {
            "number": 9,
            "triggerPosition" : 5,
            "evaluationPosition" : 13,
            "trigger": "wieder",
            "verifying": names['wieder'][8]['v_name'] + " hat des Öfteren eine Rede vor mehr als hundert Menschen gehalten.",
            "falsifying": names['wieder'][8]['f_name'] + " hat bisher nur Reden vor weniger als hundert Menschen gehalten.",
            "neutral": "Dirk hat des Öfteren eine Rede vor mehr als hundert Menschen gehalten.",
            "test-v": "Letzten Samstag hat " + names['wieder'][8]['v_name'] + " wieder eine Rede vor mehr als hundert Menschen gehalten und hat viel Applaus bekommen.",
            "test-f": "Letzten Samstag hat " + names['wieder'][8]['f_name'] + " wieder eine Rede vor mehr als hundert Menschen gehalten und hat viel Applaus bekommen.",
            "test-n": "Letzten Samstag hat " + names['wieder'][8]['n_name'] + " wieder eine Rede vor mehr als hundert Menschen gehalten und hat viel Applaus bekommen."
        },
        {
            "number": 10,
            "triggerPosition" : 4,
            "evaluationPosition" : 8,
            "trigger": "wieder",
            "verifying": names['wieder'][9]['v_name'] + " hat in seinem Leben viele Blind Dates gehabt.",
            "falsifying": names['wieder'][9]['f_name'] + " hat bis zum heutigen Tag Blind Dates gemieden.",
            "neutral": "Fritz hat bis zum heutigen Tag Blind Dates gemieden.",
            "test-v": "Heute hat " + names['wieder'][9]['v_name'] + " wieder ein Blind Date gehabt und es war enttäuschend.",
            "test-f": "Heute hat " + names['wieder'][9]['f_name'] + " wieder ein Blind Date gehabt und es war enttäuschend.",
            "test-n": "Heute hat " + names['wieder'][9]['n_name'] + " wieder ein Blind Date gehabt und es war enttäuschend."
        },
        {
            "number": 11,
            "triggerPosition" : 5,
            "evaluationPosition" : 8,
            "trigger": "wieder",
            "verifying": names['wieder'][10]['v_name'] + " ist letztes Jahr ins Ausland gereist. ",
            "falsifying": names['wieder'][10]['f_name'] + " hat bis jetzt auf Reisen ins Ausland verzichtet.",
            "neutral": "Jörg hat bis jetzt auf Reisen ins Ausland verzichtet.",
            "test-v": "Dieses Jahr ist " + names['wieder'][10]['v_name'] + " wieder ins Ausland gereist und hat Urlaub in Brasilien gemacht.",
            "test-f": "Dieses Jahr ist " + names['wieder'][10]['f_name'] + " wieder ins Ausland gereist und hat Urlaub in Brasilien gemacht.",
            "test-n": "Dieses Jahr ist " + names['wieder'][10]['n_name'] + " wieder ins Ausland gereist und hat Urlaub in Brasilien gemacht."
        },
        {
            "number": 12,
            "triggerPosition" : 5,
            "evaluationPosition" : 8,
            "trigger": "wieder",
            "verifying": names['wieder'][11]['v_name'] + " hat häufiger eine Kostümparty veranstaltet.",
            "falsifying": names['wieder'][11]['f_name'] + " hat in der Vergangenheit nie eine Kostümparty veranstaltet.",
            "neutral": "Jörg hat in der Vergangenheit nie eine Kostümparty veranstaltet.",
            "test-v": "Letzte Woche hat " + names['wieder'][11]['v_name'] + " wieder eine Kostümparty veranstaltet und ist als Käfer gegangen.",
            "test-f": "Letzte Woche hat " + names['wieder'][11]['f_name'] + " wieder eine Kostümparty veranstaltet und ist als Käfer gegangen.",
            "test-n": "Letzte Woche hat Karl wieder eine Kostümparty veranstaltet und ist als Käfer gegangen."
        },
        {
            "number": 13,
            "triggerPosition" : 2,
            "evaluationPosition" : 8,
            "trigger": "wissen",
            "verifying": names['wissen'][0]['v_name'] + " ist in Inge verliebt.",
            "falsifying": names['wissen'][0]['f_name'] + " ist nicht in Inge verliebt.",
            "neutral": "Jonas ist nicht in Inge verliebt.",
            "test-v": "Inge weiß, dass " + names['wissen'][0]['v_name'] + " in sie verliebt ist und freut sich.",
            "test-f": "Inge weiß, dass " + names['wissen'][0]['f_name'] + " in sie verliebt ist und freut sich.",
            "test-n": "Inge weiß, dass " + names['wissen'][0]['n_name'] + " in sie verliebt ist und freut sich."
        },
        {
            "number": 14,
            "triggerPosition" : 2,
            "evaluationPosition" : 7,
            "trigger": "wissen",
            "verifying": names['wissen'][1]['v_name'] + " hat eine Katze.",
            "falsifying": names['wissen'][1]['f_name'] + " hat keine Katze.",
            "neutral": "Moritz hat einen Hund.",
            "test-v": "Inge weiß, dass " + names['wissen'][1]['v_name'] + " eine Katze hat und findet das gut.",
            "test-f": "Inge weiß, dass " + names['wissen'][1]['f_name'] + " eine Katze hat und findet das gut.",
            "test-n": "Inge weiß, dass " + names['wissen'][1]['n_name'] + " eine Katze hat und findet das gut."
        },
        {
            "number": 15,
            "triggerPosition" : 2,
            "evaluationPosition" : 7,
            "trigger": "wissen",
            "verifying": names['wissen'][2]['v_name'] + " hat einen Bruder.",
            "falsifying": names['wissen'][2]['f_name'] + " hat keinen Bruder.",
            "neutral": "Moritz hat eine Schwester.",
            "test-v": "Vera weiß, dass " + names['wissen'][2]['v_name'] + " einen Bruder hat und erzählt das Susanne.",
            "test-f": "Vera weiß, dass " + names['wissen'][2]['f_name'] + " einen Bruder hat und erzählt das Susanne.",
            "test-n": "Vera weiß, dass " + names['wissen'][2]['n_name'] + "einen Bruder hat und erzählt das Susanne."
        },
        {
            "number": 16,
            "triggerPosition" : 2,
            "evaluationPosition" : 6,
            "trigger": "wissen",
            "verifying": names['wissen'][3]['v_name'] + " ist krank.",
            "falsifying": names['wissen'][3]['f_name'] + " ist nicht krank.",
            "neutral": "Markus ist nicht krank.",
            "test-v": "Tina weiß, dass " + names['wissen'][3]['v_name'] + " krank ist und macht sich Sorgen.",
            "test-f": "Tina weiß, dass " + names['wissen'][3]['f_name'] + " krank ist und macht sich Sorgen.",
            "test-n": "Tina weiß, dass " + names['wissen'][3]['n_name'] + " krank ist und macht sich Sorgen."
        },
        {
            "number": 17,
            "triggerPosition" : 2,
            "evaluationPosition" : 9,
            "trigger": "wissen",
            "verifying": names['wissen'][4]['v_name'] + " kann mit den Ohren wackeln.",
            "falsifying": names['wissen'][4]['f_name'] + " kann nicht mit den Ohren wackeln.",
            "neutral": "Karl kann mit den Ohren wackeln.",
            "test-v": "Anna weiß, dass " + names['wissen'][4]['v_name'] + " mit den Ohren wackeln kann und findet das erstaunlich.",
            "test-f": "Anna weiß, dass " + names['wissen'][4]['f_name'] + " mit den Ohren wackeln kann und findet das erstaunlich.",
            "test-n": "Anna weiß, dass " + names['wissen'][4]['n_name'] + " mit den Ohren wackeln kann und findet das erstaunlich."
        },
        {
            "number": 18,
            "triggerPosition" : 2,
            "evaluationPosition" : 7,
            "trigger": "wissen",
            "verifying": names['wissen'][5]['v_name'] + " hat eine Liebhaberin.",
            "falsifying": names['wissen'][5]['f_name'] + " hat keine Liebhaberin.",
            "neutral": "Karl hat eine Liebhaberin.",
            "test-v": "Inge weiß, dass " + names['wissen'][5]['v_name'] + " eine Liebhaberin hat und behält das für sich.",
            "test-f": "Inge weiß, dass " + names['wissen'][5]['f_name'] + " eine Liebhaberin hat und behält das für sich.",
            "test-n": "Inge weiß, dass " + names['wissen'][5]['n_name'] + " eine Liebhaberin hat und behält das für sich."
        },
        {
            "number": 19,
            "triggerPosition" : 2,
            "evaluationPosition" : 7,
            "trigger": "wissen",
            "verifying": names['wissen'][6]['v_name'] + " hat eine Tätowierung.",
            "falsifying": names['wissen'][6]['f_name'] + " hat keine Tätowierung.",
            "neutral": "Dominic hat keine Tätowierung.",
            "test-v": "Inge weiß, dass " + names['wissen'][6]['v_name'] + " eine Tätowierung hat und findet so etwas schön.",
            "test-f": "Inge weiß, dass " + names['wissen'][6]['f_name'] + " eine Tätowierung hat und findet so etwas schön.",
            "test-n": "Inge weiß, dass " + names['wissen'][6]['n_name'] + " eine Tätowierung hat und findet so etwas schön."
        },
        {
            "number": 20,
            "triggerPosition" : 2,
            "evaluationPosition" : 7,
            "trigger": "wissen",
            "verifying": names['wissen'][7]['v_name'] + " kann gut tanzen.",
            "falsifying": names['wissen'][7]['f_name'] + " kann nicht gut tanzen.",
            "neutral": "Karl kann gut tanzen.",
            "test-v": "Vera weiß, dass " + names['wissen'][7]['v_name'] + " gut tanzen kann und will mit ihm tanzen.",
            "test-f": "Vera weiß, dass " + names['wissen'][7]['f_name'] + " gut tanzen kann und will mit ihm tanzen.",
            "test-n": "Vera weiß, dass " + names['wissen'][7]['n_name'] + " gut tanzen kann und will mit ihm tanzen."
        },
        {
            "number": 21,
            "triggerPosition" : 2,
            "evaluationPosition" : 7,
            "trigger": "wissen",
            "verifying": names['wissen'][8]['v_name'] + " hat eine Stiefmutter.",
            "falsifying": names['wissen'][8]['f_name'] + " hat keine Stiefmutter.",
            "neutral": "Hans hat eine Stiefmutter.",
            "test-v": "Tina weiß, dass " + names['wissen'][8]['v_name'] + " eine Stiefmutter hat und erzählt es Fritz.",
            "test-f": "Tina weiß, dass " + names['wissen'][8]['f_name'] + " eine Stiefmutter hat und erzählt es Fritz.",
            "test-n": "Tina weiß, dass " + names['wissen'][8]['n_name'] + " eine Stiefmutter hat und erzählt es Fritz."
        },
        {
            "number": 22,
            "triggerPosition" : 2,
            "evaluationPosition" : 7,
            "trigger": "wissen",
            "verifying": names['wissen'][9]['v_name'] + " hat ein Auto.",
            "falsifying": names['wissen'][9]['f_name'] + " hat kein Auto.",
            "neutral": "Karl hat ein Auto.",
            "test-v": "Susanne weiß, dass " + names['wissen'][9]['v_name'] + " ein Auto hat und bittet ihn sie mitzunehmen.",
            "test-f": "Susanne weiß, dass " + names['wissen'][9]['f_name'] + " ein Auto hat und bittet ihn sie mitzunehmen.",
            "test-n": "Susanne weiß, dass " + names['wissen'][9]['n_name'] + " ein Auto hat und bittet ihn sie mitzunehmen."
        },
        {
            "number": 23,
            "triggerPosition" : 2,
            "evaluationPosition" : 6,
            "trigger": "wissen",
            "verifying": names['wissen'][10]['v_name'] + " hat Depressionen.",
            "falsifying": names['wissen'][10]['f_name'] + " hat keine Depressionen.",
            "neutral": "Fritz hat Depressionen.",
            "test-v": "Nadine weiß, dass " + names['wissen'][10]['v_name'] + " Depressionen hat und empfiehlt ihm einen Arzt.",
            "test-f": "Nadine weiß, dass " + names['wissen'][10]['f_name'] + " Depressionen hat und empfiehlt ihm einen Arzt.",
            "test-n": "Nadine weiß, dass " + names['wissen'][10]['n_name'] + " Depressionen hat und empfiehlt ihm einen Arzt."
        },
        {
            "number": 24,
            "triggerPosition" : 2,
            "evaluationPosition" : 6,
            "trigger": "wissen",
            "verifying": names['wissen'][11]['v_name'] + " ist geschieden.",
            "falsifying": names['wissen'][11]['f_name'] + " ist nicht geschieden.",
            "neutral": "Jonas ist nicht geschieden.",
            "test-v": "Nadine weiß, dass " + names['wissen'][11]['v_name'] + " geschieden ist und stellt ihm eine Kollegin vor.",
            "test-f": "Nadine weiß, dass " + names['wissen'][11]['f_name'] + " geschieden ist und stellt ihm eine Kollegin vor.",
            "test-n": "Nadine weiß, dass " + names['wissen'][11]['n_name'] + " geschieden ist und stellt ihm eine Kollegin vor."
        },
        {
            "number": 25,
            "triggerPosition" : 4,
            "evaluationPosition" : 5,
            "trigger": "sein",
            "verifying": names['sein'][0]['v_name'] + " hat ein Buch geschrieben.",
            "falsifying": names['sein'][0]['f_name'] + " hat kein Buch geschrieben.",
            "neutral": names['sein'][0]['n_name'] + " hat ein Gedicht geschrieben.",
            "test-v": "Maria kauft sich sein Buch und liest es.",
            "test-f": "Maria kauft sich sein Buch und liest es.",
            "test-n": "Maria kauft sich sein Buch und liest es."
        },
        {
            "number": 26,
            "triggerPosition" : 5,
            "evaluationPosition" : 6,
            "trigger": "sein",
            "verifying": names['sein'][1]['v_name'] + " hat eine Distel.",
            "falsifying": names['sein'][1]['f_name'] + " hat keine Distel.",
            "neutral": names['sein'][1]['n_name'] + " hat einen Dackel.",
            "test-v": "Steffi sticht sich an seiner Distel und flucht.",
            "test-f": "Steffi sticht sich an seiner Distel und flucht.",
            "test-n": "Steffi sticht sich an seiner Distel und flucht."
        },
        {
            "number": 27,
            "triggerPosition" : 4,
            "evaluationPosition" : 5,
            "trigger": "sein",
            "verifying": names['sein'][2]['v_name'] + " besitzt ein Restaurant.",
            "falsifying": names['sein'][2]['f_name'] + " besitzt kein Restaurant.",
            "neutral": names['sein'][2]['n_name'] + " besitzt einen Friseursalon.",
            "test-v": "Susanne geht in sein Restaurant und trifft Bekannte.",
            "test-f": "Susanne geht in sein Restaurant und trifft Bekannte.",
            "test-n": "Susanne geht in sein Restaurant und trifft Bekannte."
        },
        {
            "number": 28,
            "triggerPosition" : 3,
            "evaluationPosition" : 4,
            "trigger": "sein",
            "verifying": names['sein'][3]['v_name'] + " hat eine Vespa.",
            "falsifying": names['sein'][3]['n_name'] + " hat keine Vespa.",
            "neutral": names['sein'][3]['n_name'] + " hat einen Volvo.",
            "test-v": "Tina sieht seine Vespa und bewundert sie.",
            "test-f": "Tina sieht seine Vespa und bewundert sie.",
            "test-n": "Tina sieht seine Vespa und bewundert sie."
        },
        {   
            "number": 29,
            "triggerPosition" : 5,
            "evaluationPosition" : 6,
            "trigger": "sein",
            "verifying": names['sein'][4]['v_name'] + " besitzt eine Gärtnerei.",
            "falsifying": names['sein'][4]['f_name'] + " besitzt keine Gärtnerei.",
            "neutral": names['sein'][4]['n_name'] + " besitzt einen Bauernhof.",
            "test-v": "Laura besucht ihn in seiner Gärtnerei und geht mit ihm Mittagessen.",
            "test-f": "Laura besucht ihn in seiner Gärtnerei und geht mit ihm Mittagessen.",
            "test-n": "Laura besucht ihn in seiner Gärtnerei und geht mit ihm Mittagessen."
        },
        {
            "number": 30,
            "triggerPosition" : 3,
            "evaluationPosition" : 4,
            "trigger": "sein",
            "verifying": names['sein'][5]['v_name'] + " hat eine Katze.",
            "falsifying": names['sein'][5]['f_name'] + " hat keine Katze.",
            "neutral": names['sein'][5]['n_name'] + " hat eine kleine Schwester.",
            "test-v": "Susanne streichelt seine Katze und findet sie süß.",
            "test-f": "Susanne streichelt seine Katze und findet sie süß.",
            "test-n": "Susanne streichelt seine Katze und findet sie süß."
        },
        {
            "number": 31,
            "triggerPosition" : 4,
            "evaluationPosition" : 5,
            "trigger": "sein",
            "verifying": names['sein'][6]['v_name'] + " besitzt ein Taxi.",
            "falsifying": names['sein'][6]['f_name'] + " besitzt kein Taxi.",
            "neutral": names['sein'][6]['n_name'] + " besitzt ein Fahrrad.",
            "test-v": "Anna leiht sich sein Taxi und fährt nach Potsdam.",
            "test-f": "Anna leiht sich sein Taxi und fährt nach Potsdam.",
            "test-n": "Anna leiht sich sein Taxi und fährt nach Potsdam."
        },
        {
            "number": 32,
            "triggerPosition" : 3,
            "evaluationPosition" : 4,
            "trigger": "sein",
            "verifying": names['sein'][7]['v_name'] + " hat einen Hund.",
            "falsifying": names['sein'][7]['f_name'] + " hat keinen Hund.",
            "neutral": names['sein'][7]['n_name'] + " hat einen Wellensittich.",
            "test-v": "Inge wäscht seinen Hund und füttert ihn.",
            "test-f": "Inge wäscht seinen Hund und füttert ihn.",
            "test-n": "Inge wäscht seinen Hund und füttert ihn."
        },
        {
            "number": 33,
            "triggerPosition" : 3,
            "evaluationPosition" : 4,
            "trigger": "sein",
            "verifying": names['sein'][8]['v_name'] + " hat einen Fernseher.",
            "falsifying": names['sein'][8]['f_name'] + " hat keinen Fernseher.",
            "neutral": names['sein'][8]['n_name'] + " hat ein Radios.",
            "test-v": "Susanne repariert seinen Fernseher und er ist ihr dankbar.",
            "test-f": "Susanne repariert seinen Fernseher und er ist ihr dankbar.",
            "test-n": "Susanne repariert seinen Fernseher und er ist ihr dankbar."
        },
        {
            "number": 34,
            "triggerPosition" : 3,
            "evaluationPosition" : 4,
            "trigger": "sein",
            "verifying": names['sein'][9]['v_name'] + " hat zwei Orchideen.",
            "falsifying": names['sein'][9]['f_name'] + " hat keine Orchideen.",
            "neutral": names['sein'][9]['n_name'] + " hat einen Apfelbaum.",
            "test-v": "Tina gießt seine Orchideen und düngt sie.",
            "test-f": "Tina gießt seine Orchideen und düngt sie.",
            "test-n": "Tina gießt seine Orchideen und düngt sie."
        },
        {
            "number": 35,
            "triggerPosition" : 3,
            "evaluationPosition" : 4,
            "trigger": "sein",
            "verifying": names['sein'][10]['v_name'] + " hat einen Laptop.",
            "falsifying": names['sein'][10]['f_name'] + " hat keinen Laptop.",
            "neutral": names['sein'][10]['n_name'] + " hat ein Mobiltelefon.",
            "test-v": "Inge leiht seinen Laptop aus und bedankt sich mit einem Essen.",
            "test-f": "Inge leiht seinen Laptop aus und bedankt sich mit einem Essen.",
            "test-n": "Inge leiht seinen Laptop aus und bedankt sich mit einem Essen."
        },
        {
            "number": 36,
            "triggerPosition" : 3,
            "evaluationPosition" : 4,
            "trigger": "sein",
            "verifying": names['sein'][11]['v_name'] + " trägt eine Brille.",
            "falsifying": names['sein'][11]['f_name'] + " trägt keine Brille.",
            "neutral": names['sein'][11]['n_name'] + " trägt keinen Hut.",
            "test-v": "Susanne bewundert seine Brille und macht ihm ein Kompliment.",
            "test-f": "Susanne bewundert seine Brille und macht ihm ein Kompliment.",
            "test-n": "Susanne bewundert seine Brille und macht ihm ein Kompliment."
        },
        {
            "number": 37,
            "triggerPosition" : 4,
            "evaluationPosition" : 11,
            "trigger": "auch",
            "verifying": "Karl hat ein Geschenk für " + names['auch'][0]['v_name'] + " mitgebracht.",
            "falsifying": "Niemand hat ein Geschenk für " + names['auch'][0]['f_name'] + " mitgebracht.",
            "neutral": "Karl hat keinen Salat für " + names['auch'][0]['n_name'] + " mitgebracht.",
            "test-v": names['auch'][0]['v_name'] + " vermutet, dass auch Fritz ein Geschenk für sie mitgebracht hat und ist deswegen fröhlich.",
            "test-f": names['auch'][0]['f_name'] + " vermutet, dass auch Fritz ein Geschenk für sie mitgebracht hat und ist deswegen fröhlich.",
            "test-n": names['auch'][0]['n_name'] + " vermutet, dass auch Fritz ein Geschenk für sie mitgebracht hat und ist deswegen fröhlich."
        },
        {
            "number": 38,
            "triggerPosition" : 4,
            "evaluationPosition" : 11,
            "trigger": "auch",
            "verifying": "Fritz kocht heute eine Suppe mit " + names['auch'][1]['v_name'] + ".",
            "falsifying": "Niemand kocht heute eine Suppe mit " + names['auch'][1]['f_name'] + ".",
            "neutral": "Niemand isst heute ein Eis mit " + names['auch'][1]['n_name'] + ".",
            "test-v": names['auch'][1]['v_name'] + " hofft, dass auch Susanne heute eine Suppe mit ihr kocht und kauft dafür Zutaten.",
            "test-f": names['auch'][1]['f_name'] + " hofft, dass auch Susanne heute eine Suppe mit ihr kocht und kauft dafür Zutaten.",
            "test-n": names['auch'][1]['n_name'] + " hofft, dass auch Susanne heute eine Suppe mit ihr kocht und kauft dafür Zutaten."
        },
        {
            "number": 39,
            "triggerPosition" : 4,
            "evaluationPosition" : 9,
            "trigger": "auch",
            "verifying": "Karl schuldet " + names['auch'][2]['v_name']+ " noch Geld.",
            "falsifying": "Niemand schuldet " + names['auch'][2]['f_name'] + " noch Geld.",
            "neutral": "Karl schuldet " + names['auch'][2]['n_name'] + " noch einen Gefallen.",
            "test-v": names['auch'][2]['v_name'] + " glaubt, dass auch Inge ihr noch Geld schuldet und ruft sie an.",
            "test-f": names['auch'][2]['f_name'] + " glaubt, dass auch Inge ihr noch Geld schuldet und ruft sie an.",
            "test-n": names['auch'][2]['n_name'] + " glaubt, dass auch Inge ihr noch Geld schuldet und ruft sie an."
        },
        {
            "number": 40,
            "triggerPosition" : 4,
            "evaluationPosition" : 10,
            "trigger": "auch",
            "verifying": "Fritz schreibt einen Song für " + names['auch'][3]['v_name'] + ".",
            "falsifying": "Niemand schreibt einen Song für " + names['auch'][3]['f_name'] + ".",
            "neutral": "Niemand schreibt ein Gedicht für " +  names['auch'][3]['n_name'] + ".",
            "test-v": names['auch'][3]['v_name'] + " hofft, dass auch Karl einen Song für sie schreibt und ist gespannt.",
            "test-f": names['auch'][3]['f_name'] + " hofft, dass auch Karl einen Song für sie schreibt und ist gespannt.",
            "test-n": names['auch'][3]['n_name'] + " hofft, dass auch Karl einen Song für sie schreibt und ist gespannt."
        },
        {
            "number": 41,
            "triggerPosition" : 4,
            "evaluationPosition" : 11,
            "trigger": "auch",
            "verifying": "Heute machen alle " + names['auch'][4]['v_name'] + " ein Kompliment.",
            "falsifying": "Heute macht niemand " + names['auch'][4]['f_name'] + " ein Kompliment.",
            "neutral": "Heute macht Fritz " + names['auch'][4]['n_name'] + " einen Heiratsantrag.",
            "test-v": names['auch'][4]['v_name'] + " nimmt an, dass auch Karl ihr heute ein Kompliment machen wird und wartet ungeduldig.",
            "test-f": names['auch'][4]['f_name'] + " nimmt an, dass auch Karl ihr heute ein Kompliment machen wird und wartet ungeduldig.",
            "test-n": names['auch'][4]['n_name'] + " nimmt an, dass auch Karl ihr heute ein Kompliment machen wird und wartet ungeduldig."
        },
        {
            "number": 42,
            "triggerPosition" : 4,
            "evaluationPosition" : 12,
            "trigger": "auch",
            "verifying": "Fritz schenkt " + names['auch'][5]['v_name'] + " ein Kochbuch zu Weihnachten.",
            "falsifying": "Niemand schenkt " + names['auch'][5]['f_name'] + " Kochbuch zu Weihnachten.",
            "neutral": "Alle schenken " + names['auch'][5]['n_name'] + " Plätzchen zu Weihnachten.",
            "test-v": names['auch'][5]['v_name'] + " fürchtet, dass auch Tina ihr ein Kochbuch zu Weihnachten schenken wird und ist betrübt.",
            "test-f": names['auch'][5]['f_name'] + " fürchtet, dass auch Tina ihr ein Kochbuch zu Weihnachten schenken wird und ist betrübt.",
            "test-n": names['auch'][5]['n_name'] + " fürchtet, dass auch Tina ihr ein Kochbuch zu Weihnachten schenken wird und ist betrübt."
        },
        {
            "number": 43,
            "triggerPosition" : 5,
            "evaluationPosition" : 11,
            "trigger": "auch",
            "verifying": "Fritz kocht am Wochenende für " + names['auch'][6]['v_name'] + ".",
            "falsifying": "Niemand kocht am Wochenende für " + names['auch'][6]['f_name'] + ".",
            "neutral": "Fritz singt am Wochenende für " + names['auch'][6]['n_name'] + ".",
            "test-v": names['auch'][6]['v_name'] + " wünscht sich, dass auch Peter am Wochenende für sie kocht und sagt ihm das.",
            "test-f": names['auch'][6]['f_name'] + " wünscht sich, dass auch Peter am Wochenende für sie kocht und sagt ihm das.",
            "test-n": names['auch'][6]['n_name'] + " wünscht sich, dass auch Peter am Wochenende für sie kocht und sagt ihm das."
        },
        {
            "number": 44,
            "triggerPosition" : 4,
            "evaluationPosition" : 10,
            "trigger": "auch",
            "verifying": "Niemand sammelt Pilze für " + names['auch'][7]['v_name'] + " .",
            "falsifying": "Karl sammelt Pilze für " + names['auch'][7]['f_name'] + ".",
            "neutral": names['auch'][7]['v_name'] + " sammelt Briefmarken für Karl.",
            "test-v": names['auch'][7]['v_name'] + " vermutet, dass auch Anton keine Pilze für sie sammelt und geht selbst welche suchen.",
            "test-f": names['auch'][7]['f_name'] + " vermutet, dass auch Anton keine Pilze für sie sammelt und geht selbst welche suchen.",
            "test-n": names['auch'][7]['n_name'] + " vermutet, dass auch Anton keine Pilze für sie sammelt und geht selbst welche suchen."
        },
        {           
            "number": 45,
            "triggerPosition" : 4,
            "evaluationPosition" : 8,
            "trigger": "auch",
            "verifying": "Karl findet " + names['auch'][8]['v_name'] + " unsympathisch.",
            "falsifying": "Alle finden " + names['auch'][8]['f_name'] + " sympathisch.",
            "neutral": "Karl findet " + names['auch'][8]['n_name'] + " gutaussehend.",
            "test-v": "Tina denkt, dass auch Fritz " + names['auch'][8]['v_name'] + " unsympathisch findet und kann das nicht verstehen.",
            "test-f": "Tina denkt, dass auch Fritz " + names['auch'][8]['f_name'] + " unsympathisch findet und kann das nicht verstehen.",
            "test-n": "Tina denkt, dass auch Fritz " + names['auch'][8]['n_name'] + " unsympathisch findet und kann das nicht verstehen."
        },
        {
            "number": 46,
            "triggerPosition" : 4,
            "evaluationPosition" : 10,
            "trigger": "auch",
            "verifying": "Karl findet " + names['auch'][9]['v_name']  + "s neue Frisur schrecklich.",
            "falsifying": "Alle finden " + names['auch'][9]['f_name']  + "s neue Frisur gut.",
            "neutral": "Alle finden " + names['auch'][9]['n_name']  + "s neue Tasche gut.",
            "test-v": names['auch'][9]['v_name'] + " befürchtet, dass auch Fritz ihre neue Frisur schrecklich findet und versteckt sich.",
            "test-f": names['auch'][9]['f_name'] + " befürchtet, dass auch Fritz ihre neue Frisur schrecklich findet und versteckt sich.",
            "test-n": names['auch'][9]['n_name'] + " befürchtet, dass auch Fritz ihre neue Frisur schrecklich findet und versteckt sich."
        },
        {
            "number": 47,
            "triggerPosition" : 4,
            "evaluationPosition" : 12,
            "trigger": "auch",
            "verifying": "Anton hat heute eine schlechte Nachricht für " + names['auch'][10]['v_name'] + ".",
            "falsifying": "Niemand hat heute eine schlechte Nachricht für " + names['auch'][10]['f_name'] + ".",
            "neutral": "Peter hat heute eine interessante Nachricht für " + names['auch'][10]['n_name'] + ".",
            "test-v": names['auch'][10]['v_name'] + " ahnt, dass auch Susanne heute eine schlechte Nachricht für sie hat und meidet sie.",
            "test-f": names['auch'][10]['f_name'] + " ahnt, dass auch Susanne heute eine schlechte Nachricht für sie hat und meidet sie.",
            "test-n": names['auch'][10]['n_name'] + " ahnt, dass auch Susanne heute eine schlechte Nachricht für sie hat und meidet sie."
        },
        {
            "number": 48,
            "triggerPosition" : 6,
            "evaluationPosition" : 12,
            "trigger": "auch",
            "verifying": "Fritz kauft eine Kaffeemaschine für " + names['auch'][11]['v_name'] + ".",
            "falsifying": "Niemand kauft eine Kaffeemaschine für "+ names['auch'][11]['f_name'] +".",
            "neutral": " Niemand kauft einen Wasserkocher für "+ names['auch'][11]['n_name'] + ".",
            "test-v": names['auch'][11]['v_name'] + " geht davon aus, dass auch Tina eine Kaffeemaschine für sie kauft und will sie davon abhalten.",
            "test-f": names['auch'][11]['f_name'] + " geht davon aus, dass auch Tina eine Kaffeemaschine für sie kauft und will sie davon abhalten.",
            "test-n": names['auch'][11]['n_name'] + " geht davon aus, dass auch Tina eine Kaffeemaschine für sie kauft und will sie davon abhalten."
        },
        {
            "number": 49,
            "triggerPosition" : 3,
            "evaluationPosition" : 6,
            "trigger": "aufhören",
            "verifying": names['aufhören'][0]['v_name'] + " hilft in einem Altersheim aus.",
            "falsifying": names['aufhören'][0]['f_name'] + " hilft nicht in einem Altersheim aus.",
            "neutral": " Fritz hilft in einem Altersheim aus.",
            "test-v": names['aufhören'][0]['v_name'] + " wird aufhören im Altersheim auszuhelfen und teilt dies einem Vorgesetzten mit.",
            "test-f": names['aufhören'][0]['f_name'] + " wird aufhören im Altersheim auszuhelfen und teilt dies einem Vorgesetzten mit.",
            "test-n": names['aufhören'][0]['n_name'] + " wird aufhören im Altersheim auszuhelfen und teilt dies einem Vorgesetzten mit."
        },
        {
            "number": 50,
            "triggerPosition" : 3,
            "evaluationPosition" : 7,
            "trigger": "aufhören",
            "verifying": names['aufhören'][1]['v_name'] + " geht abends oft joggen.",
            "falsifying": names['aufhören'][1]['f_name'] + " geht abends nie joggen.",
            "neutral": "Karl geht abends nie joggen.",
            "test-v": names['aufhören'][1]['v_name'] + " wird aufhören abends joggen zu gehen und meldet sich zum Yoga an.",
            "test-f": names['aufhören'][1]['f_name'] + " wird aufhören abends joggen zu gehen und meldet sich zum Yoga an.",
            "test-n": names['aufhören'][1]['n_name'] + " wird aufhören abends joggen zu gehen und meldet sich zum Yoga an."
        },
        {
            "number": 51,
            "triggerPosition" : 5,
            "evaluationPosition" : 9,
            "trigger": "aufhören",
            "verifying": "Susanne und " + names['aufhören'][11]['v_name'] + " gehen oft gemeinsam Tanzen.",
            "falsifying": "Susanne und " + names['aufhören'][11]['f_name'] + " gehen nie gemeinsam Tanzen.",
            "neutral": "Tina und Karl gehen oft gemeinsam Tanzen.",
            "test-v": "Susanne und " + names['aufhören'][11]['v_name'] + " werden aufhören gemeinsam Tanzen zu gehen, weil Fritz Knieprobleme hat.",
            "test-f": "Susanne und " + names['aufhören'][11]['f_name'] + " werden aufhören gemeinsam Tanzen zu gehen, weil Fritz Knieprobleme hat.",
            "test-n": "Susanne und " + names['aufhören'][11]['n_name'] + " werden aufhören gemeinsam Tanzen zu gehen, weil Fritz Knieprobleme hat."
        },
        {
            "number": 52,
            "triggerPosition" : 3,
            "evaluationPosition" : 6,
            "trigger": "aufhören",
            "verifying": names['aufhören'][2]['v_name'] + " sammelt Briefmarken.",
            "falsifying": names['aufhören'][2]['f_name'] + " sammelt keine Briefmarken.",
            "neutral": "Karl sammelt keine Briefmarken.",
            "test-v": names['aufhören'][2]['v_name'] + " wird aufhören Briefmarken zu sammeln, weil es zu teuer ist.",
            "test-f": names['aufhören'][2]['f_name'] + " wird aufhören Briefmarken zu sammeln, weil es zu teuer ist.",
            "test-n": names['aufhören'][2]['n_name'] + " wird aufhören Briefmarken zu sammeln, weil es zu teuer ist."
        },
        {
            "number": 53,
            "triggerPosition" : 3,
            "evaluationPosition" : 9,
            "trigger": "aufhören",
            "verifying": names['aufhören'][3]['v_name'] + " hat einen alten Corsa, den er nie abschließt.",
            "falsifying": names['aufhören'][3]['f_name'] + " hat einen alten Corsa, den er immer abschließt.",
            "neutral": "Jonas hat einen alten Corsa, den er nie abschließt.",
            "test-v": names['aufhören'][3]['v_name'] + " wird aufhören seinen Corsa offen stehen zu lassen, weil er Angst vor Dieben hat.",
            "test-f": names['aufhören'][3]['f_name'] + " wird aufhören seinen Corsa offen stehen zu lassen, weil er Angst vor Dieben hat.",
            "test-n": names['aufhören'][3]['n_name'] + " wird aufhören seinen Corsa offen stehen zu lassen, weil er Angst vor Dieben hat."
        },
        {
            "number": 54,
            "triggerPosition" : 3,
            "evaluationPosition" : 5,
            "trigger": "aufhören",
            "verifying": names['aufhören'][4]['v_name'] + " ist Raucher.",
            "falsifying": names['aufhören'][4]['f_name'] + " ist Nichtraucher.",
            "neutral": "Moritz ist Nichtraucher.",
            "test-v": names['aufhören'][4]['v_name'] + " wird aufhören zu rauchen, weil es ungesund ist.",
            "test-f": names['aufhören'][4]['f_name'] + " wird aufhören zu rauchen, weil es ungesund ist.",
            "test-n": names['aufhören'][4]['n_name'] + " wird aufhören zu rauchen, weil es ungesund ist."
        },
        {
            "number": 55,
            "triggerPosition" : 3,
            "evaluationPosition" : 7,
            "trigger": "aufhören",
            "verifying": names['aufhören'][5]['v_name'] + " isst kein Fleisch.",
            "falsifying": names['aufhören'][5]['f_name'] + " isst täglich Fleisch.",
            "neutral": "Dominic isst kein Fleisch.",
            "test-v": names['aufhören'][5]['v_name'] + " wird aufhören sich vegetarisch zu ernähren, weil er Eisenmangel hat.",
            "test-f": names['aufhören'][5]['f_name'] + " wird aufhören sich vegetarisch zu ernähren, weil er Eisenmangel hat.",
            "test-n": names['aufhören'][5]['n_name'] + " wird aufhören sich vegetarisch zu ernähren, weil er Eisenmangel hat."
        },
        {
            "number": 56,
            "triggerPosition" : 3,
            "evaluationPosition" : 7,
            "trigger": "aufhören",
            "verifying": names['aufhören'][6]['v_name'] + " geht oft auf Partys.",
            "falsifying": names['aufhören'][6]['f_name'] + " geht nie auf Partys.",
            "neutral": "Karl geht oft auf Partys.",
            "test-v": names['aufhören'][6]['v_name'] + " wird aufhören auf Partys zu gehen, weil er mehr lernen muss.",
            "test-f": names['aufhören'][6]['f_name'] + " wird aufhören auf Partys zu gehen, weil er mehr lernen muss.",
            "test-n": names['aufhören'][6]['n_name'] + " wird aufhören auf Partys zu gehen, weil er mehr lernen muss."
        },
        {
            "number": 57,
            "triggerPosition" : 3,
            "evaluationPosition" : 7,
            "trigger": "aufhören",
            "verifying": names['aufhören'][7]['v_name'] + " macht zurzeit eine Diät.",
            "falsifying": names['aufhören'][7]['f_name'] + " macht zurzeit keine Diät.",
            "neutral": "Jonas macht zurzeit eine Diät.",
            "test-v": names['aufhören'][7]['v_name'] + " wird aufhören eine Diät zu machen, weil er Süßes zu sehr vermisst.",
            "test-f": names['aufhören'][7]['f_name'] + " wird aufhören eine Diät zu machen, weil er Süßes zu sehr vermisst.",
            "test-n": names['aufhören'][7]['n_name'] + " wird aufhören eine Diät zu machen, weil er Süßes zu sehr vermisst."
        },
        {
            "number": 58,
            "triggerPosition" : 3,
            "evaluationPosition" : 7,
            "trigger": "aufhören",
            "verifying": names['aufhören'][8]['v_name'] + " macht täglich Sport.",
            "falsifying": names['aufhören'][8]['f_name'] + " macht nie Sport.",
            "neutral": "Moritz macht täglich Sport.",
            "test-v": names['aufhören'][8]['v_name'] + " wird aufhören täglich Sport zu machen, weil er Knieprobleme hat.",
            "test-f": names['aufhören'][8]['f_name'] + " wird aufhören täglich Sport zu machen, weil er Knieprobleme hat.",
            "test-n": names['aufhören'][8]['n_name'] + " wird aufhören täglich Sport zu machen, weil er Knieprobleme hat."
        },
        {
            "number": 59,
            "triggerPosition" : 5,
            "evaluationPosition" : 8,
            "trigger": "aufhören",
            "verifying": names['aufhören'][9]['v_name'] + " isst nie Fastfood.",
            "falsifying": names['aufhören'][9]['f_name'] + " isst nur Fastfood.",
            "neutral": "Jan isst nie Fastfood.",
            "test-v": names['aufhören'][9]['v_name'] + " wird ab jetzt aufhören Fastfood zu essen, weil er dick geworden ist.",
            "test-f": names['aufhören'][9]['f_name'] + " wird ab jetzt aufhören Fastfood zu essen, weil er dick geworden ist.",
            "test-n": names['aufhören'][9]['n_name'] + " wird ab jetzt aufhören Fastfood zu essen, weil er dick geworden ist."
        },          
        {
            "number": 60,
            "triggerPosition" : 3,
            "evaluationPosition" : 6,
            "trigger": "aufhören",
            "verifying": names['aufhören'][10]['v_name'] + " spendet nie Geld für soziale Zwecke.",
            "falsifying": names['aufhören'][10]['f_name'] + " spendet immer Geld für soziale Zwecke.",
            "neutral": "Hans spendet oft Geld für soziale Zwecke.",
            "test-v": names['aufhören'][10]['v_name'] + " wird aufhören Spendenaufrufe zu ignorieren, weil er ein schlechtes Gewissen hat.",
            "test-f": names['aufhören'][10]['f_name'] + " wird aufhören Spendenaufrufe zu ignorieren, weil er ein schlechtes Gewissen hat.",
            "test-n": names['aufhören'][10]['n_name'] + " wird aufhören Spendenaufrufe zu ignorieren, weil er ein schlechtes Gewissen hat."
        }
    ];

    for (var i = 0; i < items.length; i++) {
        groups[Math.floor(i / 12)].push(items[i]);
    }

    for (var i = 0; i < groups.length; i++) {
        for (var j = 0; j < groups[i].length; j++) {
            lists[Math.floor(j % 6)].push(groups[i][j]);
        }
    }

    return lists[Math.floor(Math.random() * lists.length)];
};