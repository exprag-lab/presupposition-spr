$("document").ready(function() {
    // prevent scrolling when space is pressed
    window.onkeydown = function(e) {
        if (e.keyCode == 32 && e.target == document.body) {
            e.preventDefault();
        }
    };

    const trials = prepareTrials();

    babeInit({
        views_seq: createViews(trials),
        deploy: {
            experimentID: '17',
            serverAppURL: 'https://mcmpact.ikw.uni-osnabrueck.de/babe/api/submit_experiment/',
            deployMethod: 'Prolific',
            contact_email: 'exprag@gmail.com',
            prolificURL: 'https://app.prolific.ac/submissions/complete?cc=MZ48BXJ5'
        },
        progress_bar: {
            in: [
                'practice',
                'main'
            ],
            style: "separate",
            width: 100
        }
    })
});